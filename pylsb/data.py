"""Basic data that we need, and can't easily be gathered from the website."""

__version__ = '1.0.0'

BOOKS = [
    'genesis', 'exodus', 'leviticus', 'numbers', 'deuteronomy',

    'joshua', 'judges', 'ruth', '1samuel', '2samuel', '1kings', '2kings',
    '1chronicles', '2chronicles', 'ezra', 'nehemiah', 'esther',

    'job', 'psalm', 'proverbs', 'ecclesiastes', 'song of songs',

    'isaiah', 'jeremiah', 'lamentations', 'ezekiel', 'daniel',

    'hosea', 'joel', 'amos', 'obadiah', 'jonah', 'micah', 'nahum', 'habakkuk',
    'zephaniah', 'haggai', 'zechariah', 'malachi',


    'matthew', 'mark', 'luke', 'john', 'acts',

    'romans', '1corinthians', '2corinthians', 'galatians', 'ephesians',
    'philippians', 'colossians', '1thessalonians', '2thessalonians',

    '1timothy', '2timothy', 'titus', 'philemon', 'hebrews',

    'james', '1peter', '2peter', '1john', '2john', '3john', 'jude',

    'revelation',
]

TITLES = [
    'GENESIS', 'EXODUS', 'LEVITICUS', 'NUMBERS', 'DEUTERONOMY',

    'The Book of\nJOSHUA', 'The Book of\nJUDGES', 'The Book of\nRUTH',
    'The First Book of\nSAMUEL', 'The Second Book of\nSAMUEL',
    'The First Book of the\nKINGS', 'The Second Book of the\nKINGS',
    'The First Book of the\nCHRONICLES', 'The Second Book of the\nCHRONICLES',
    'The Book of\nEZRA', 'The Book of\nNEHEMIAH', 'The Book of\nESTHER',

    'The Book of\nJOB', 'THE PSALMS', 'PROVERBS', 'The Book of\nECCLESIASTES',
    'THE SONG OF SONGS',

    'The Book of\nISAIAH', 'The Book of\nJEREMIAH',
    'THE LAMENTATIONS\nof Jeremiah', 'The Book of\nEZEKIEL',
    'The Book of\nDANIEL',

    'The Book of\nHOSEA', 'The Book of\nJOEL', 'The Book of\nAMOS',
    'The Book of\nOBADIAH', 'The Book of\nJONAH', 'The Book of\nMICAH',
    'The Book of\nNAHUM', 'The Book of\nHABAKKUK', 'The Book of\nZEPHANIAH',
    'The Book of\nHAGGAI', 'The Book of\nZECHARIAH', 'The Book of\nMALACHI',


    'The Gospel According to\nMATTHEW', 'The Gospel According to\nMARK',
    'The Gospel According to\nLUKE', 'The Gospel According to\nJOHN',
    'THE ACTS\nof the Apostles',

    'The Letter of Paul to the\nROMANS',
    'The First Letter of Paul to the\nCORINTHIANS',
    'The Second Letter of Paul to the\nCORINTHIANS',
    'The Letter of Paul to the\nGALATIANS',
    'The Letter of Paul to the\nEPHESIANS',
    'The Letter of Paul to the\nPHILIPPIANS',
    'The Letter of Paul to the\nCOLOSSIANS',
    'The First Letter of Paul to the\nTHESSALONIANS',
    'The Second Letter of Paul to the\nTHESSALONIANS',

    'The First Letter of Paul to\nTIMOTHY',
    'The Second Letter of Paul to\nTIMOTHY', 'The Letter of Paul to\nTITUS',
    'The Letter of Paul to\nPHILEMON', 'The Letter to the\nHEBREWS',

    'The Letter of\nJAMES', 'The First Letter of\nPETER',
    'The Second Letter of\nPETER', 'The First Letter of\nJOHN',
    'The Second Letter of\nJOHN', 'The Third Letter of\nJOHN',
    'The Letter of\nJUDE',

    'THE REVELATION\nto John',
]

CHAPTERS = [
    50, 40, 27, 36, 34,

    24, 21, 4, 31, 24, 22, 25, 29, 36, 10, 13, 10,

    42, 150, 31, 12, 8,

    66, 52, 5, 48, 12,

    14, 3, 9, 1, 4, 7, 3, 3, 3, 2, 14, 4,


    28, 16, 24, 21, 28,

    16, 16, 13, 6, 6, 4, 4, 5, 3,

    6, 4, 3, 1, 13,

    5, 5, 3, 5, 1, 1, 1,

    22,
]
