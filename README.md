# PyLSB
This library provides a convenient way to retrieve the content of the Legacy
Standard Bible. It is platform agnostic, saves data into an easy to parse JSON
file, and retrieves all data from the official website
(https://read.lsbible.org/).

If you're looking for the command line LSB viewing utility, that has been split
off into [its own project](https://gitlab.com/Magicrafter13/pylsb-cli).

This software is not officially endorsed by, or affiliated with, The Lockman
Foundation or Three Sixteen Publishing Inc. (the copyright holders of the LSB)
in any way. I hold no responsibility for your usage of this software. It was
created as a hobby/mild-learning project for myself as this is the translation I
personally read. To support the translators and printers, consider
[purchasing](https://316publishing.com/collections/lsb) your own copy of the
LSB.

This is living documentation. I've never created a library before. Please bear
with me, and feel free to submit merge requests with suggested changes.

# How to Use
Each module is detailed below:

## pylsb.bible
This is the primary module, and is intended to mainly be interacted with via a
`BibleGetter` object. Some simple data classes are provided as well, which are
`BibleMarker` and `BibleRange`.

### BibleMarker
This data class simply stores the book, chapter, and verse number of a
particular verse in Scripture.

### BibleRange
This data class stores two `BibleMarker` classes, and is intended to be used to
form inclusive ranges, though may be used for any purpose where two verses are
needed.

### BibleGetter
This class is instantiated with a string argument, which supplies the path to a
file where data will be stored and read back from upon subsequent use (the file
will be created if it does not already exist).

- - -

The most important function is `set_useragent(_ua)`, because I want to be kind
to the people hosting https://read.lsbible.org. Whatever string (`_ua`) you pass
to this function, is what will be sent to the web server when downloading
Scripture, as part of the client's user-agent. Best practice is to use your
email if this is a personal project, otherwise a project maintainer address
could be best, or you may want to add user side functionality to change this.

This will have no effect on this software's operation, but is done as a gesture
towards those who would make this data freely available on the web, despite it
being available for purchase.

- - -

The main function you'll probably use is `get`, and possibly, `get_chapters` and
`get_verses`.

First, `get(selection, redownload)` is used to retrieve the data. `selection` is
either a `BibleMarker` or `BibleRange` of the verse(s) you want, and
`redownload` is a boolean specifying whether or not to force update the local
data (if the data isn't downloaded yet, this doesn't change anything, it will
still download). This function returns a sub-dictionary of what's stored
overall, but with strings instead of integers for chapter/verse numbers, so
things will be returned in the following format:

```json
{
    "matthew": {
        "28": {
            "19": { ... },
            "20": { ... }
        }
    },
    "mark": {
        "1": {
            "1": { ... },
            "2": { ... }
        }
    }
}
```

(This is an example of a `BibleRange` between Matthew 28:19 and Mark 1:2.)

Secondarily you may wish to check how many chapters are in a book, or how many
verse are in a chapter. For this you will use `get_chapters(book)` or
`get_verses(book, chapter)` respectively. This should be self explanatory, but
`book` is the book you want to check, and `chapter` is an integer of the chapter
you want to check. These both return an integer.

- - -

After those, the most important function is `save()`, which simply writes out
the Bible data in memory to disk. If you do not call this, none of what's
changed since the `BibleGetter` was instantiated will be committed to disk.

This function will only write the file if the data in memory has in fact
changed, if no changes have been made it will ignore this call, so it is safe to
call this each time you use the class (baring any raised exceptions).

- - -

There is a pair of methods for searching the entire loaded Bible for attributes,
either backwards from a point, or forwards. These functions are
`find_attribute_backwards` and `find_attribute_forwards` respectively. They both
take the same arguments: `start, attr, inclusive, redownload` where `start` is a
`BibleMarker` of the beginning of the search, `attr` is the exact attribute to
search for, `inclusive` is a boolean specifying whether or not to include
`start` in the search, and `redownload` is a boolean specifying whether each
verse should be re-downloaded before being searched. Some of these attributes
may be post-fixed with a '#' character, followed by some additional data. The
following attributes currently exist in some form:

- paragraph: indicates the start of a new paragraph (bold verse number in print
copies)
- quote: indicate the verse begins with a quotation of mark of some kind - the
format is `quote#x#y` where x is the line number (0 indexed) and y is the
quotation type (currently 1 indicates single quote and 2 indicates double
quote) - the quotation is still in the actual text itself, this is only useful
for visual formatting.
- indent: indicates indented text with the format `indent#x#y` where x is the
line number (0 indexed) and y is the indentation level (currently there is 1 for
block quotes and 2 for indented block quotes) - like with quote, this is purely
for visual formatting.
- poetry: just another print styling like quote and indent, currently there is
only level 1, but the format remains the same `poetry#x#y` as the above.
- break: indicates spacing between this and the previous verse.
- acrostic: indicates the verse starts a new Hebrew letter in an acrostic poem.
(with the letter in red above the verse in print copies) - each verse with this
should also have an additional `acrostic#...` attribute, where `...` is the
Hebrew letter.
- selah: some poetic prose (right margin in print copies) - there can be more
than one of these per verse, so each of these attributes is post-fixed with
`#n#1` where n is the line number (0 indexed) that ends with this. I don't
recall why the `#1` was added, but it currently is hard coded in and has no
meaning.
- higgaion-selah: a unique selah I've only seen in one verse... (identical to
selah, but with higgaion)
- highlight

These two functions return a `BibleMarker` pointing to the first verse found,
with the attribute given.

- - -

If you're working with user input, you may wish to use the `valid(selection)`
function. This takes a `BibleMarker` or `BibleRange` and will return true if it
is valid, or false otherwise. The criteria that determines validity is simple;
for a `BibleMarker` it just has to be a verse that exists, and for a
`BibleRange`, it has to have both of its child `BibleMarker`s be valid.
`BibleRange` is not checked to see if its first verse comes before its last.

- - -

Lastly, there is `highlight(book, chapter, verse, color)`. This simply adds (or
changes) the highlight attribute to a given verse. Technically it just sets an
integer value, which is converted to a string, so any number is valid, however
0, 1-255, and -1 through -0xFFFFFF are the only values that will be "supported"
as the original intent was for true color terminal color values. I don't know
why this doesn't take a `BibleMarker` as the first argument...

### expand_book
This module also has a function of the form `expand_book(book)` where book is a
string containing a possible book title. As long as the string passed matches
the start of a valid book, and only one book, that book will be returned. If no
matches were found, `None` is returned, otherwise an empty string is returned to
indicate too many possible matches.

This should probably be moved to [the parse module](#pylsb.parse) to be honest.

## pylsb.parse
Most people should only need `get_scripture(arg, bible)`, which takes a string
(`arg`) and attempts to parse it into either a `BibleMarker` or `BibleRange`
(the return value), using the given `bible` (`BibleGetter`). This function can
raise `ValueError` (because of `parse_scripture`), or based on its own validity
checks, can raise `argparse.ArgumentTypeError`. Since checking for valid input
requires having data that does not ship with this library, if necessary, the
`bible` passed to this function will download the appropriate chapters.

I may wish to change that exception type, since `argparse` is likely only
relevant to the CLI application...

That function relies heavily on `parse_scripture(arg, bible)` which is also
available to the developer should they desire more control. This function simply
extracts the data passed to it using the regex abomination called `BIBLE_REGEX`
which you may study if you have an afternoon to burn, but essentially this
returns a `BibleRange` that may or may not be valid data. This function checks
if the book(s) specified exist, and if they have as many chapters as would be
required for the `arg` to be valid, but that is all. It also fails if the regex
fails to match of course. All failures from this function will be raised
`ValueError` exceptions.

- - -

`get_highlight` and `args` should likely be moved out of this library.

## pylsb.data
Most developers probably won't need to use this, but this module simply contains
3 arrays. `BOOKS` contains all 66 book names, while `TITLES` contains their full
display titles (I could not find this data on the site in the way I wanted it,
so created it from my printed copy). `CHAPTERS` contains the chapter counts for
each book - this was a manageable task, but creating an array of the number of
verses per chapter seemed excessive.

## pylsb.utils
This is silly and I should probably find a way to make it a private module or
something, I'm not really sure how to approach this. I won't bother documenting
the functions they're quite elementary, just look at the source file.
